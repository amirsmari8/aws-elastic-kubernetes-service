## Introduction
- The Kubernetes Vertical Pod Autoscaler automatically adjusts the CPU and memory reservations for your pods to help "right size" your applications. 
- This adjustment can improve cluster resource utilization and free up CPU and memory for other pods. 

## Requirements
- Metric server should be installed & ready, we have done that as part of HPA

## Deploy the Vertical Pod Autoscaler (VPA)

# Clone Repo
git clone https://github.com/kubernetes/autoscaler.git

# Navigate to VPA
cd autoscaler/vertical-pod-autoscaler/

# Uninstall VPA (if we are using old one)
./hack/vpa-down.sh

# Install new version of VPA
./hack/vpa-up.sh

# Verify VPA Pods
kubectl get pods -n kube-system 

######################## 

kubectl get vpa
1. VPA Updater can re-launch new pod with updated CPU and Memory when you atleast have 2 pods in a deployment. 
2. If we have only one pod, unless we manually delete that pod, it will not launch new pod with VPA recommended CPU and memory considerign the application availability scenario.