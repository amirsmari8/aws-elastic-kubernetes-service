# Download IAM Policy
## Download latest
curl -o iam_policy_latest.json https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/main/docs/install/iam_policy.json 


# Create IAM Policy using policy downloaded 
aws iam create-policy \
    --policy-name AWSLoadBalancerControllerIAMPolicy \
    --policy-document file://iam_policy_latest.json


the arn of the policies: arn:aws:iam::921605632850:policy/AWSLoadBalancerControllerIAMPolicy 

## Step-03: Create an IAM role for the AWS LoadBalancer Controller and attach the role to the Kubernetes service account 
- Applicable only with `eksctl` managed clusters
- This command will create an AWS IAM role 
- This command also will create Kubernetes Service Account in k8s cluster
- In addition, this command will bound IAM Role created and the Kubernetes service account created
### Step-03-01: Create IAM Role using eksctl (see note below)

eksctl create iamserviceaccount \
  --cluster=eks-micro \
  --namespace=kube-system \
  --name=aws-load-balancer-controller \
  --attach-policy-arn=arn:aws:iam::921605632850:policy/AWSLoadBalancerControllerIAMPolicy \
  --override-existing-serviceaccounts \
  --approve 

## Note: 
- To enable and use AWS IAM roles for Kubernetes service accounts on our EKS cluster, we must create &  associate OIDC identity provider. 

eksctl utils associate-iam-oidc-provider \
    --region us-east-2 \
    --cluster eks-micro \
    --approve


# Verify if any existing service account
kubectl get sa -n kube-system    
kubectl get sa aws-load-balancer-controller -n kube-system






## Step-04: Install the AWS Load Balancer Controller using Helm V3 
helm repo add eks https://aws.github.io/eks-charts
helm repo update

## note 
- [Get Region Code and Account info](https://docs.aws.amazon.com/eks/latest/userguide/add-ons-images.html)



helm install aws-load-balancer-controller eks/aws-load-balancer-controller \
  -n kube-system \
  --set clusterName=eks-micro \
  --set serviceAccount.create=false \
  --set serviceAccount.name=aws-load-balancer-controller \
  --set region=us-east-2 \
  --set vpcId=vpc-01cbf4ef156c43bbc \
  --set image.repository=602401143452.dkr.ecr.us-east-2.amazonaws.com/amazon/aws-load-balancer-controller 


# Verify that the controller is installed.
kubectl -n kube-system get deployment 
kubectl -n kube-system get deployment aws-load-balancer-controller
kubectl -n kube-system describe deployment aws-load-balancer-controller 

# Uninstall AWS Load Balancer Controller
helm uninstall aws-load-balancer-controller -n kube-system 



# ingressClassName : 
https://kubernetes.github.io/ingress-nginx/user-guide/multiple-ingress/ 

