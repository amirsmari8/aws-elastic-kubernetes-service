- Init Containers run **before** App containers
- Init containers can contain **utilities or setup scripts** not present in an app image.
- We can have and run **multiple Init Containers** before App Container. 
- Init containers are exactly like regular containers, **except:**
  - Init containers always **run to completion.**
  - Each init container must **complete successfully** before the next one starts.
- If a Pod's init container fails, Kubernetes repeatedly restarts the Pod until the init container succeeds.
- However, if the Pod has a `restartPolicy of Never`, Kubernetes does not restart the Pod.

---------------------------------------------------------------------------------------------------- 
** Healthchecks are Probes that apply to containers (NOT a POD) 
liveness: is the containers dead or alive ? 
readiness: is the container ready to serve traffic ?
startup: is the conatiner still startup ?

https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/ 
different probes handlers are availables: HTTP, TCP, program execution => see Doc officiel 

## liveness command or program execution : 
livenessProbe:
      exec:
        command:
        - cat
        - /tmp/healthy
      initialDelaySeconds: 5
      periodSeconds: 5 
#if the command succeeds, it returns 0, and the kubelet considers the container to be alive and healthy. If the command returns a non-zero value, the kubelet kills the container and restarts it.       

## liveness HTTP request 
livenessProbe:
      httpGet:
        path: /healthz
        port: 8080
        httpHeaders:
        - name: Custom-Header
          value: Awesome 
#To perform a probe, the kubelet sends an HTTP GET request to the server that is running in the container and listening on port 8080. If the handler for the server's /healthz path returns a success code, the kubelet considers the container to be alive and healthy.     

## TCP liveness probe          
livenessProbe:
      tcpSocket:
        port: 8080
#The kubelet will send the first readiness probe 5 seconds after the container starts. This will attempt to connect to the goproxy container on port 8080. 

## gRPC liveness probe 
livenessProbe:
      grpc:
        port: 2379 
        




The kubelet uses liveness probes to know when to restart a container.  
The kubelet uses readiness probes to know when a container is ready to start accepting traffic. A Pod is considered ready when all of its containers are ready. One use of this signal is to control which Pods are used as backends for Services. When a Pod is not ready, it is removed from Service load balancers. 


-------------------------------------------------------------------------------------------------------- 
https://linuxhint.com/nc-command-examples/
The nc or netcat command is a network tool that allows users to transfer files between devices, scan ports and diagnose problems 


- if readinessProbe fails the pods will be in state of Ready (0/1) and restarted 0 
- if liveness probe is fails , the pod will be restart restarted (>0)