## Introduction
- The Kubernetes Cluster Autoscaler automatically adjusts the number of nodes in your cluster when pods fail to launch due to lack of resources or when nodes in the cluster are underutilized and their pods can be rescheduled onto other nodes in the cluster. 

## Verify if our NodeGroup as --asg-access
Verify if our NodeGroup as --asg-access
- We need to ensure that we have a parameter named `--asg-access` present during the cluster or nodegroup creation.
- Verify the same when we created our cluster node group 


## Deploy Cluster Autoscaler
```
# Deploy the Cluster Autoscaler to your cluster
kubectl apply -f https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml

# Add the cluster-autoscaler.kubernetes.io/safe-to-evict annotation to the deployment
kubectl -n kube-system annotate deployment.apps/cluster-autoscaler cluster-autoscaler.kubernetes.io/safe-to-evict="false"
```

## Step-04: Edit Cluster Autoscaler Deployment to add Cluster name and two more parameters
```
kubectl -n kube-system edit deployment.apps/cluster-autoscaler
```
- **Add cluster name**
```yml
# Before Change
        - --node-group-auto-discovery=asg:tag=k8s.io/cluster-autoscaler/enabled,k8s.io/cluster-autoscaler/<YOUR CLUSTER NAME>

# After Change
        - --node-group-auto-discovery=asg:tag=k8s.io/cluster-autoscaler/enabled,k8s.io/cluster-autoscaler/eks-micro
```

- **Add two more parameters**
```yml
        - --balance-similar-node-groups
        - --skip-nodes-with-system-pods=false
```
- **Sample for reference**
```yml
    spec:
      containers:
      - command:
        - ./cluster-autoscaler
        - --v=4
        - --stderrthreshold=info
        - --cloud-provider=aws
        - --skip-nodes-with-local-storage=false
        - --expander=least-waste
        - --node-group-auto-discovery=asg:tag=k8s.io/cluster-autoscaler/enabled,k8s.io/cluster-autoscaler/eks-micro
        - --balance-similar-node-groups
        - --skip-nodes-with-system-pods=false
```

## Set the Cluster Autoscaler Image related to our current EKS Cluster version
- Open https://github.com/kubernetes/autoscaler/releases
- Find our release version (example: 1.16.n) and update the same. 
- Our Cluster version is 1.16 and our cluster autoscaler version is 1.16.5 as per above releases link 
``` 


# Template
# Update Cluster Autoscaler Image Version
kubectl -n kube-system set image deployment.apps/cluster-autoscaler cluster-autoscaler=us.gcr.io/k8s-artifacts-prod/autoscaling/cluster-autoscaler:v1.XY.Z


# Update Cluster Autoscaler Image Version
kubectl -n kube-system set image deployment.apps/cluster-autoscaler cluster-autoscaler=us.gcr.io/k8s-artifacts-prod/autoscaling/cluster-autoscaler:v1.16.5
```