- Instead of specifying `resources like cpu and memory` in every container spec of a pod defintion, we can provide the default CPU & Memory for all containers in a namespace using `LimitRange` 


all containers will get request equal to Limit range. 
** Test LimitRange

$kubectl get limits -n dev  

$kubectl describe limits ns-resource-quota -n dev 

** Test ResourceQuota 

kubectl get quota -n dev
kubectl describe quota ns-resource-quota -n dev