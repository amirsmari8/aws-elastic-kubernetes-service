### Rollout New Deployment - Set Image Option 
kubectl set image deployment notification-microservice notification-service=stacksimplify/kube-notifications-microservice:2.0.0 
notification-service: name of the container 
# Verify Rollout Status
kubectl rollout status deployment notification-microservice

# Verify ReplicaSets
kubectl get rs

# Verify Rollout History
kubectl rollout history deployment notification-microservice 

# Roll back to Previous Version
kubectl rollout undo deployment/notification-microservice



## Rollout New Deployment - kubectl Edit 
kubectl edit deployment notification-microservice 

## Rollout New Deployment - Update manifest & kubectl apply 



